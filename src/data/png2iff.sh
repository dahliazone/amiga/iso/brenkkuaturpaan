#!/bin/bash

echo "imagemagick: \"$1\" -> \"${1%.png}.32.png\""
convert "$1" -colors 32 -depth 6 "${1%.png}.32.png"

echo "gfxconv: \"${1%.png}.32.png\" -> \"${1%.png}.iff\""
gfxconv "${1%.png}.32.png" -o "${1%.png}.iff" -P -I

