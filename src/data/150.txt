name=Brenkkua turpaan
group=Karmar & Reeta
party=Bukkasyksy
year=2018
comment=Loimaa domination 2018
music=brenkkua.mod
debug=0
info=0
#resolution=320x200x8
#resolution=320x200x16
resolution=320x200x32
#resolution=320x240x8
#resolution=320x240x16
#resolution=320x240x32

# files should be 320x200 at 32 colors Amiga IFF LBM format

turpaan.iff 25
turpaan2.iff 25
turpaan.iff 25
turpaan2.iff 25
turpaan.iff 25
turpaan2.iff 25
turpaan.iff 25
turpaan2.iff 25
turpaan.iff 25
turpaan2.iff 25
turpaan.iff 25
turpaan2.iff 25
turpaan.iff 25
turpaan2.iff 25

huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25

patteriin6.iff 25
patteriin7.iff 25
patteriin2.iff 25
patteriin3.iff 25
patteriin6.iff 25
patteriin7.iff 25
patteriin2.iff 50
patteriin3.iff 50
patteriin6.iff 25
patteriin7.iff 25
patteriin2.iff 25
patteriin3.iff 25
patteriin6.iff 25
patteriin7.iff 25

nyrkki.iff 50
nyrkki2.iff 50
nyrkki.iff 50
nyrkki2.iff 50
nyrkki.iff 50
nyrkki2.iff 50
nyrkki.iff 50
nyrkki2.iff 50

huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25
huoria.iff 25
huoria2.iff 25

