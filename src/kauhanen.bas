Auto View On 

Hide 

Screen Open 0,320,200,32,Lowres : Curs Off : Flash Off : Screen Hide : Screen 0 : Cls 0 : Ink 2,0
Screen Show 0
Screen To Front 

Auto View Off 

DD$="data/"

Set Input 10,-1
Open In 1,DD$+"150.txt"

RESW=320
RESH=200
RESCOL=32
DEMONAME$=""
DEMOGROUP$=""
DEMOPARTY$=""
DEMOYEAR$=""
DEMOCOMMENT$=""
DEMOMUSIC$=""
DEMOINFO=True
DEMODEBUG=False

Dim DEMOIMAGE$(64)
Dim DEMOIMAGEUSAGE(64)
DEMOIMAGECOUNT=0
Dim DTLIMAGE(256)
Dim DTLDUR(256)
Dim DTLENDTIME(256)
DTLCOUNT=0
TIMESUM=0

While Not Eof(1)
   Line Input #1,L$

   R$=""
   If(Len(L$)=0)
      'R$="empty"
      Goto SKIPLINE
   Else If(Asc(Left$(L$,1))=Asc("#"))
      'R$="comment"
      Goto SKIPLINE
   Else If(Left$(L$,5)="name=")
      DEMONAME$=Mid$(L$,6,Len(L$)-5)
      'R$="name = "+DEMONAME$
   Else If(Left$(L$,6)="group=")
      DEMOGROUP$=Mid$(L$,7,Len(L$)-6)
      'R$="group = "+DEMOGROUP$
   Else If(Left$(L$,6)="party=")
      DEMOPARTY$=Mid$(L$,7,Len(L$)-6)
      'R$="party = "+DEMOPARTY$  
   Else If(Left$(L$,5)="year=")
      DEMOYEAR$=Mid$(L$,6,Len(L$)-5)
      'R$="year = "+DEMOYEAR$
   Else If(Left$(L$,8)="comment=")
      DEMOCOMMENT$=Mid$(L$,9,Len(L$)-8)
      'R$="comment = "+DEMOCOMMENT$
   Else If(Left$(L$,6)="music=")
      DEMOMUSIC$=Mid$(L$,7,Len(L$)-6)
      'R$="music = "+DEMOMUSIC$
   Else If(Left$(L$,5)="info=")
      If(Mid$(L$,6,Len(L$)-5)<>"0")
         DEMOINFO=True
      End If 
   Else If(Left$(L$,6)="debug=")
      If(Mid$(L$,7,Len(L$)-6)<>"0")
         DEMODEBUG=True
      End If 
   Else If(Left$(L$,11)="resolution=")
      RES$=Mid$(L$,12,Len(L$)-11)
      If(RES$="320x200x8")
         RESW=320 : RESH=200 : RESCOL=8
      Else If(RES$="320x200x16")
         RESW=320 : RESH=200 : RESCOL=16
      Else If(RES$="320x200x32")
         RESW=320 : RESH=200 : RESCOL=32
      Else If(RES$="320x240x8")
         RESW=320 : RESH=240 : RESCOL=8
      Else If(RES$="320x240x16")
         RESW=320 : RESH=240 : RESCOL=16
      Else If(RES$="320x240x32")
         RESW=320 : RESH=240 : RESCOL=32
      End If 
   Else 
      TMP=Instr(L$," ")

      DEMOIMAGEFILETEMP$=Left$(L$,TMP-1)
      DUPLICATE=False
      For I=0 To DEMOIMAGECOUNT
         If(DEMOIMAGE$(I)=DEMOIMAGEFILETEMP$)
            DUPLICATE=True
            DUPLICATEINDEX=I
            Exit 
         End If 
      Next 

      DTLDUR(DTLCOUNT)=Val(Mid$(L$,TMP+1,Len(L$)-TMP))
      DTLENDTIME(DTLCOUNT)=TIMESUM+DTLDUR(DTLCOUNT)
      TIMESUM=TIMESUM+DTLDUR(DTLCOUNT)

      If(DUPLICATE=False)
         DEMOIMAGE$(DEMOIMAGECOUNT)=DEMOIMAGEFILETEMP$
         DTLIMAGE(DTLCOUNT)=DEMOIMAGECOUNT
         DEMOIMAGEUSAGE(DEMOIMAGECOUNT)=1
         'R$="image "+DEMOIMAGE$(DEMOIMAGECOUNT)+" "+Str$(DTLDUR(DTLCOUNT)) 
         Inc DEMOIMAGECOUNT
         Print "unique: "+L$,DTLDUR(DTLCOUNT)
      Else 
         DTLIMAGE(DTLCOUNT)=DUPLICATEINDEX
         Inc DEMOIMAGEUSAGE(DUPLICATEINDEX)
         Print "duplicate: "+DEMOIMAGEFILETEMP$,DTLDUR(DTLCOUNT)
      End If 

      Inc DTLCOUNT
   End If 

   SKIPLINE:
   If R$<>"" Then Print L$,R$
Wend 
Close 1

If(DEMODEBUG=True)
   Print "music:"+DEMOMUSIC$+":"
   View 
   Wait 50
End If 

Screen Open 0,RESW,RESH,RESCOL,Lowres : Curs Off : Flash Off : Screen Hide : Screen 0 : Cls 0
Screen Open 1,RESW,RESH,RESCOL,Lowres : Curs Off : Flash Off : Screen Hide : Screen 1 : Cls 0
'Screen Open 2,RESW,RESH,RESCOL,Lowres : Curs Off : Flash Off : Screen Hide : Screen 2 : Cls 0 
'Screen Open 3,RESW,RESH,RESCOL,Lowres : Curs Off : Flash Off : Screen Hide : Screen 3 : Cls 0 
'Screen Open 4,RESW,RESH,RESCOL,Lowres : Curs Off : Flash Off : Screen Hide : Screen 4 : Cls 0 
'Screen Open 5,RESW,RESH,RESCOL,Lowres : Curs Off : Flash Off : Screen Hide : Screen 5 : Cls 0 
'Screen Open 6,RESW,RESH,RESCOL,Lowres : Curs Off : Flash Off : Screen Hide : Screen 6 : Cls 0 
'Screen Open 7,RESW,RESH,RESCOL,Lowres : Curs Off : Flash Off : Screen Hide : Screen 7 : Cls 0 
SCRCOUNT=2

Screen 0
Screen Show 0
Screen To Front 

Cls 2
Ink 2,0
Text 10,RESH-60," "+DEMONAME$+" by "+DEMOGROUP$+" "
Text 10,RESH-50," Year: "+DEMOYEAR$+" "
Text 10,RESH-40," Released at "+DEMOPARTY$+" "
Text 10,RESH-20," "+DEMOCOMMENT$+" "

View 
Wait 350
Screen To Back 

Bob Off : Wait Vbl 
Curs Off : Wait Vbl 

' LOAD MUSIC 
Pt Cia Speed 125
'Track Load DD$+DEMOMUSIC$,8 
Imploder Load DD$+DEMOMUSIC$,-3
Pt Play 3

DTLINDEX=0
Load Iff(DD$+DEMOIMAGE$(DTLIMAGE(DTLINDEX))),DTLINDEX
Load Iff(DD$+DEMOIMAGE$(DTLIMAGE(DTLINDEX+1))),DTLINDEX+1
INITTIME=Timer
LASTLOADTIME=0

Do 
   If Key State(69)
      Exit 
   End If 
   If Mouse Click<>0
      Exit 
   End If 

   NOWTIME=Timer-INITTIME
   If(NOWTIME>DTLENDTIME(DTLINDEX))
      Inc DTLINDEX
      If(DTLINDEX>=DTLCOUNT)
         Exit 
      End If 

      Screen To Back 
      Screen DTLINDEX mod SCRCOUNT
      Screen Show DTLINDEX mod SCRCOUNT
      Screen To Front 
      View 

      TEMP=Timer
      Load Iff(DD$+DEMOIMAGE$(DTLIMAGE(DTLINDEX+1))),(DTLINDEX+1) mod SCRCOUNT
      LASTLOADTIME=Timer-TEMP
   End If 

   If DEMODEBUG=True
      Ink 0,1
      Text 0,10,Str$(DTLINDEX+1)+" /"+Str$(DTLCOUNT)+" "
      Text 0,20," "+DEMOIMAGE$(DTLIMAGE(DTLINDEX))+" "
      Text 0,30,Str$(DTLDUR(DTLINDEX))+" frames "
      Text 0,40,"time now: "+Str$(NOWTIME)+" frames"
      Text 0,50,"load took: "+Str$(LASTLOADTIME)+" frames"
      View 
   End If 
Loop 

Pt Stop 
